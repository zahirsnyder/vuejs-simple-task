import { createRouter, createWebHistory } from 'vue-router';
import HomeUser from './components/HomeUser.vue'
import ViewUser from './components/ViewUser.vue'
import EditUser from './components/EditUser.vue'
import CreateUser from './components/CreateUser.vue';

const routes = [
  { path: '/', component: HomeUser },
  { path: '/ViewUser/:Id', name: 'ViewUser', component: ViewUser, props: true},
  { path: '/EditUser/:Id', name: 'EditUser', component: EditUser, props: true },
  { path: '/CreateUser', component: CreateUser, name: 'CreateUser' },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
